import { Response } from "./Response";

export class Question {
  id: number;
  label: string;
  responses: Response[];
  constructor(id: number, label: string, responses: Response[]) {
    this.id = id;
    this.label = label;
    this.responses = responses;
  }
}
