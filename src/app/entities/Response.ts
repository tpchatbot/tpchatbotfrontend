import { Question } from "./Question";

export class Response {
  id: number;
  label: string;
  childQuestion: Question;
  isSelected: boolean;
  isEnabled: boolean;
  constructor(id: number, label: string, childQuestion: Question) {
    this.id = id;
    this.label = label;
    this.childQuestion = childQuestion;
    this.isSelected = false;
    this.isEnabled = true;
  }
}
