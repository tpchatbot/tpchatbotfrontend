export class ConversationItem {
  question_id: Number;
  response_id: Number;
  constructor(question_id: Number, response_id: Number) {
    this.question_id = question_id;
    this.response_id = response_id;
  }
}
