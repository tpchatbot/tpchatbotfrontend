import { ConversationItem } from "./ConversationItem";
import { Question } from "./Question";
import { Response } from "./Response";

export class Conversation {
  id: number;
  items: ConversationItem[];
  constructor() {
    this.items = [];
  }
  add(question_id: Number, response_id: Number) {
    let conversationItem = new ConversationItem(question_id, response_id);
    this.items.push(conversationItem);
  }
}
