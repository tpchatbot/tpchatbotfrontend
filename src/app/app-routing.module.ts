import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ChatBotComponent } from "./components/chat-bot/chat-bot.component";

const routes: Routes = [
  { path: "chat", component: ChatBotComponent },
  { path: "", redirectTo: "/chat", pathMatch: "full" },
  { path: "**", redirectTo: "/chat", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
