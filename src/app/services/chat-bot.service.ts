import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Conversation } from "../entities/Conversation";
import { ConversationItem } from "../entities/ConversationItem";

@Injectable({
  providedIn: "root"
})
export class ChatBotService {
  constructor(private http: HttpClient) {}
  getRootQuestions() {
    let url = "/api/rootQuestion";
    return this.http.get(url);
  }
  sendConversation(conversation: Conversation) {
    let url = "/api/conversation/save";
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      })
    };
    let email = "email";
    let password = "password";
    return this.http.post<any>(url, conversation.items, httpOptions);
  }
}
