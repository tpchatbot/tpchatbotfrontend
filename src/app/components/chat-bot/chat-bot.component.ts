import { Component, OnInit } from "@angular/core";
import { ChatBotService } from "src/app/services/chat-bot.service";
import { Question } from "src/app/entities/Question";
import { Conversation } from "src/app/entities/Conversation";
import { Response } from "src/app/entities/Response";
import { retryWhen, flatMap } from "rxjs/operators";
import { interval, throwError, of } from "rxjs";

@Component({
  selector: "app-chat-bot",
  templateUrl: "./chat-bot.component.html",
  styleUrls: ["./chat-bot.component.css"]
})
export class ChatBotComponent implements OnInit {
  private questions: Question[];
  private conversation: Conversation;
  private sendingConversation: Boolean;
  constructor(private chatBotService: ChatBotService) {
    this.questions = [];
    this.conversation = new Conversation();
    this.sendingConversation = false;
  }
  onResponseClick(response: Response, question: Question) {
    if (response.isEnabled) {
      response.isSelected = true;
      this.disableClickEvent(question);
      if (response.childQuestion != null) {
        this.initSelection(response.childQuestion);
        this.questions.push(response.childQuestion);
        this.conversation.add(question.id, response.id);
      }
      if (
        response.childQuestion == null ||
        response.childQuestion.responses.length == 0
      ) {
        this.sendingConversation = true;
        this.chatBotService
          .sendConversation(this.conversation)
          .subscribe(data => {
            console.log(data);
          });
      }
    }
  }
  initSelection(question: Question) {
    question.responses.forEach(response => {
      response.isEnabled = true;
      response.isSelected = false;
    });
  }
  disableClickEvent(question: Question) {
    question.responses.forEach(response => {
      response.isEnabled = false;
    });
  }
  ngOnInit() {
    this.initChatBox();
  }
  initChatBox() {
    this.chatBotService
      .getRootQuestions()
      .pipe(
        retryWhen(_ => {
          return interval(2000).pipe(
            flatMap(count =>
              count == 5 ? throwError("Server Error !") : of(count)
            )
          );
        })
      )
      .subscribe((question: Question) => {
        this.initSelection(question);
        this.questions.push(question);
      });
  }
  onRetryBtnClick() {
    this.questions.length = 0;
    this.sendingConversation = false;
    this.initChatBox();
  }
}
