# TP CHATBOT FRONTEND

la partie FrontEnd du TP ChatBot PANDACOLA

## Technologies utilisées 

* Angular    : version 7.1.0
* TypeScript : version 3.1.6
* node       : version 8.12.0
* npm        : version 6.9.0

## Configuration et Lancement de l'application



pour lancer l'application exécuter les commandes suivantes  :

```
cd TPChatBotBackEnd

npm start

```
